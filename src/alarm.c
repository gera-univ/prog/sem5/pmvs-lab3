#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <setjmp.h>

static jmp_buf env_alarm;

static void
sig_alarm(int nsig)
{
	longjmp(env_alarm, 1);
}

int
main(void) {
	if (signal(SIGALRM, sig_alarm) == SIG_ERR)
		return 1;
	printf("alarm set\n");
	if (setjmp(env_alarm) == 0) {
		alarm(5);
		pause();
	}
	printf("signal caught\n");
	alarm(0);
	return 0;
}
