#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <setjmp.h>

void
usage(char *argv0, int exit_code)
{
	fprintf(stderr, "Usage: %s [-h] [-N depth] dir string\n", argv0);
	exit(exit_code);
}

void find(char *str, char *filename) {
	size_t str_length = strlen(str);
	int fd = open(filename, O_RDONLY);
	if (fd == -1) {
		perror("open");
		exit(-1);
	}

	const size_t buffer_size = 1024;
	char *buffer = malloc(buffer_size);
	
	pid_t pid = getpid();
	size_t i = 0, byte_count = 0;
	ssize_t bytes_read;
	while ((bytes_read = read(fd, buffer, buffer_size)) > 0) {
		for (size_t j = 0; j < buffer_size; ++j, ++byte_count) {
			if (buffer[j] == str[i])
				++i;
			else
				i = 0;
			if (i == str_length)
				printf("%d\t%lu\t%s\n", pid, byte_count, filename);
		}
	}

	free(buffer);
	close(fd);
}

int
main(int argc, char **argv)
{
	int n_max = 6;
	int opt;
	while ((opt = getopt(argc, argv, "hN:")) != -1) {
		switch (opt) {
		case 'h':
			usage(argv[0], EXIT_SUCCESS);
			break;
		case 'N':
			n_max = atoi(optarg);
			break;
		default:
			usage(argv[0], EXIT_FAILURE);
		}
	}
	if (optind >= argc - 1) {
		usage(argv[0], EXIT_FAILURE);
	}

	setvbuf(stdout, NULL, _IONBF, 0);
	printf("pid\tbytes\tfile\n");

	char *dirpath = argv[optind];
	char *needle = argv[optind + 1];

	struct dirent *dent;

	chdir(dirpath);

	jmp_buf threadstart;
	int n = setjmp(threadstart);
	if (n >= n_max)
		exit(EXIT_SUCCESS);
	DIR *dirp = opendir(".");
	if (dirp == NULL) {
		perror("opendir");
		exit(EXIT_FAILURE);
	}
	int currfd = open(".", O_RDONLY);

	pid_t child_pid = -1;
	struct stat sb;
	for (;;) {
		errno = 0;
		dent = readdir(dirp);
		if (dent == NULL)
			break;
		if (strcmp(dent->d_name, ".") == 0 || strcmp(dent->d_name, "..") == 0)
			continue;
		if (lstat(dent->d_name, &sb) == -1) {
			perror("lstat");
			exit(EXIT_FAILURE);
		}
		switch (sb.st_mode & S_IFMT) {
			case S_IFREG:
				//printf("file: %s\n", dent->d_name);
				find(needle, dent->d_name);
				break;
			case S_IFDIR:
				chdir(dent->d_name);
				//printf("dir: %s\n", dent->d_name);
				fflush(stdout);
				pid_t child_pid = fork();
				if (child_pid == -1) {
					perror("fork");
					exit(EXIT_FAILURE);
				}
				else if (child_pid == 0) {
					closedir(dirp);
					longjmp(threadstart, n + 1);
				}
				fchdir(currfd);
				break;
			default:
				break;
		}
	}
	closedir(dirp);
}
