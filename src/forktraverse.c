#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <setjmp.h>
#include <time.h>

void
usage(char *argv0, int exit_code)
{
	fprintf(stderr, "Usage: %s [-N depth] dir\n", argv0);
	exit(exit_code);
}

int
main(int argc, char **argv)
{
	printf("main start\n");

	int n_max = 6;
	int opt;
	while ((opt = getopt(argc, argv, "hN:")) != -1) {
		switch (opt) {
		case 'N':
			n_max = atoi(optarg);
			break;
		default:
			usage(argv[0], EXIT_FAILURE);
		}
	}
	if (optind >= argc) {
		usage(argv[0], EXIT_FAILURE);
	}

	#define CLOCKFD 3
	#define FD_TO_CLOCKID(fd)   ((~(clockid_t) (fd) << 3) | CLOCKFD)
	#define CLOCKID_TO_FD(clk)  ((unsigned int) ~((clk) >> 3))

	struct timespec ts;
	clockid_t clkid;
	int fd;

	setvbuf(stdout, NULL, _IONBF, 0);

	fd = open("/dev/ptp0", O_RDWR);
	clkid = FD_TO_CLOCKID(fd);

	char *dirpath = argv[optind];

	struct dirent *dent;

	chdir(dirpath);

	jmp_buf threadstart;
	char cwd[PATH_MAX];
	int n = setjmp(threadstart);
	if (n >= n_max)
		exit(EXIT_SUCCESS);

	clock_gettime(clkid, &ts);
	printf("%ld\n", ts.tv_nsec);
	if (getcwd(cwd, sizeof(cwd)) != NULL) {
		printf("Current working dir: %s\n", cwd);
	} else {
		perror("getcwd() error");
		return 1;
	}

	DIR *dirp = opendir(".");
	if (dirp == NULL) {
		perror("opendir");
		exit(EXIT_FAILURE);
	}
	int currfd = open(".", O_RDONLY);

	pid_t child_pid = -1;
	struct stat sb;
	for (;;) {
		errno = 0;
		dent = readdir(dirp);
		if (dent == NULL)
			break;
		if (strcmp(dent->d_name, ".") == 0 || strcmp(dent->d_name, "..") == 0)
			continue;
		if (lstat(dent->d_name, &sb) == -1) {
			perror("lstat");
			exit(EXIT_FAILURE);
		}
		switch (sb.st_mode & S_IFMT) {
			case S_IFDIR:
				chdir(dent->d_name);
				//printf("dir: %s\n", dent->d_name);
				pid_t child_pid = fork();
				if (child_pid == -1) {
					perror("fork");
					exit(EXIT_FAILURE);
				}
				else if (child_pid == 0) {
					closedir(dirp);
					longjmp(threadstart, n + 1);
				}
				fchdir(currfd);
				break;
			default:
				break;
		}
	}
	closedir(dirp);
}
